FROM node:20-alpine AS builder
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY ./src ./src
COPY ./public ./public
RUN npm run build

FROM nginx
ARG FRONTEND_DOMAIN
ARG BACKEND_DOMAIN
ENV FRONTEND_DOMAIN=$FRONTEND_DOMAIN
ENV BACKEND_DOMAIN=$BACKEND_DOMAIN
COPY ./nginx/default.conf.template /etc/nginx/templates/default.conf.template
COPY --from=builder /app/build /usr/share/nginx/html
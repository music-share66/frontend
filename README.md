# README

**提供中文版以及英文版，請選擇您熟悉的語言進行閱讀**

**Available in both Chinese and English. Please select the language you are comfortable with.**

**選擇語言 / Choose Your Language**

- [中文版](README.md#中文版)
- [English Version](README.md#english-version)

# 中文版

# 專案介紹

## 描述

Music Share 是一個音樂分享平台，旨在讓使用者能夠輕鬆地上傳、分享和串流播放音樂。

這邊為此專案之 Frontend repository, 詳細專案介紹請查看後端 Repository README。

- **後端 Repository**：[https://gitlab.com/music-share66/backend.git](https://gitlab.com/music-share66/backend.git)

## Demo 網址

可以在以下網址查看 Music Share：

[https://musicshare.liaojourney.com](https://musicshare.liaojourney.com/)

**注意事項**：

- 由於沒有製作響應式網頁設計 (RWD)，請以 1500x1180 以上的解析度大小顯示器開啟，以獲得良好的瀏覽體驗。
- 開放時間：09:00 GMT+8 - 18:00 GMT+8

## Demo 影片

可以在以下網址查看 Music Share 平台的 Demo 影片：

[https://www.youtube.com/watch?v=Kjj0HvCsqtA](https://www.youtube.com/watch?v=Kjj0HvCsqtA)

## 相關 Repository

這些 Repository 包含了後端應用和雲端部署相關的所有檔案：

- **後端 Repository**：[https://gitlab.com/music-share66/backend.git](https://gitlab.com/music-share66/backend.git)
- **雲端 Repository**：[https://gitlab.com/music-share66/deploy.git](https://gitlab.com/music-share66/deploy.git)

# Local 運行環境需求

在執行專案前端前，需要確保擁有以下運行環境：

- **Node.js 20.10.0**：

前端技術選擇為React，請確保安裝了 Node.js 20.10.0 或更高版本。可以從 [Node.js 官方網站](https://nodejs.org/) 下載並安裝適用的版本。

## 在 Loacl 安裝, 運行

按照以下步驟在本地環境中安裝並運行專案前端：

**Step 1. 安裝套件**：

這個指令會根據 package.json 安裝所有必要的 npm 套件。

```
npm install
```
    
**Step 2. 啟動開發環境**：

這個指令會啟動開發伺服器，您可以在瀏覽器中打開 `http://localhost:8000` 來訪問應用程式。

```
npm start
```

# English Version

# Project Introduction

## Description

Music Share is a music sharing platform designed to allow users to easily upload, share, and stream music.

This is the frontend repository for the project. For detailed project information, please refer to the README in the backend repository.

- **Backend Repository** : [https://gitlab.com/music-share66/backend.git](https://gitlab.com/music-share66/backend.git)

## Demo URL

You can view the demo of Music Share at the following URL : 

https://musicshare.liaojourney.com

**Note** :

- As there is no responsive web design (RWD), please open it on a display with a resolution of 1500x1180 or above for a better browsing experience.
- Available time: 09:00 GMT+8 - 18:00 GMT+8

## Demo Video

You can view the demo video of the Music Share platform at the following URL :

[https://www.youtube.com/watch?v=Kjj0HvCsqtA](https://www.youtube.com/watch?v=Kjj0HvCsqtA)

## Related Repositories

These repositories contain all files related to the frontend application and cloud deployment :

- **Frontend Repository** : [https://gitlab.com/music-share66/frontend.git](https://gitlab.com/music-share66/frontend.git)
- **Cloud Repository** : [https://gitlab.com/music-share66/deploy.git](https://gitlab.com/music-share66/deploy.git)

# Local Running Environment Requirements

Before running the project backend, ensure that you have the following running environment:

- **Node.js 20.10.0** :

The frontend technology chosen is React, so make sure you have Node.js 20.10.0 or higher installed. You can download and install the appropriate version from the Node.js official website.

## Installing and Running Locally

Follow these steps to install and run the backend of the project in your local environment:

**Step 1. Install Dependencies** : 

This command will install all necessary npm packages according to the package.json file.

```
npm install
```
    
**Step 2. Start Development Server** :

This command will start the development server. You can access the application in your browser at http://localhost:8000.

```
npm run dev
``` 

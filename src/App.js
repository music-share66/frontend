import { Routes, Route } from 'react-router-dom';
// import Play from './pages/Play/Play.jsx';
import Signin from './pages/Signin/Signin.jsx';
import Signup from './pages/Signup/Signup.jsx';
import Index from './pages/Index/Index.jsx';
import Page404 from './pages/Page404/Page404.jsx';

function App() {
  return (
    <Routes>
      <Route path='/*' element={<Index />} />
      <Route path='/signin' element={<Signin />} />
      <Route path='/signup' element={<Signup />} />
      <Route path='*' element={<Page404 />} />
    </Routes>
  );
}

export default App;

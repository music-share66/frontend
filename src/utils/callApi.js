import { useState } from 'react';
import axios from 'axios';
import apiHost from './apiHostConfig';

axios.defaults.withCredentials = true;

export const useGet = () => {
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(null);
  const [data, setData] = useState(null);


  const getData = async (path, header = null) => {
    setIsLoading(true);

    const result = await axios({
      method: 'GET',
      url: `${apiHost}${path}`,
      header,
      validateStatus: () => true,
    });

    const resultData = result.data;

    if (resultData.status === 'error') {
      setIsLoading(false);
      setError(resultData);
    }

    if (resultData.status === 'success') {
      setIsLoading(false);
      setData(resultData.result);
    }
  };

  return { getData, setError, data, isLoading, error };
};

export const usePost = () => {
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(null);
  const [response, setResponse] = useState(null);

  const postData = async (path, body, header = null) => {
    setIsLoading(true);

    const result = await axios({
      method: 'POST',
      url: `${apiHost}${path}`,
      data: body,
      header,
      validateStatus: () => true,
    });

    const resultData = result.data;

    if (resultData.status === 'error') {
      setIsLoading(false);
      setError(resultData);
    }

    if (resultData.status === 'success') {
      setIsLoading(false);
      setResponse(resultData.result);
    }
  };

  return { postData, setError, response, isLoading, error };
};

export const usePut = () => {
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(null);
  const [response, setResponse] = useState(null);

  const putData = async (path, body, header = null) => {
    setIsLoading(true);

    const result = await axios({
      method: 'PUT',
      url: `${apiHost}${path}`,
      data: body,
      header,
      validateStatus: () => true,
    });

    const resultData = result.data;

    if (resultData.status === 'error') {
      setIsLoading(false);
      setError(resultData);
    }

    if (resultData.status === 'success') {
      setIsLoading(false);
      setResponse(resultData.result);
    }
  };

  return { putData, setError, response, isLoading, error };
};

export const useDelete = () => {
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(null);
  const [response, setResponse] = useState(null);

  const deleteData = async (path, body, header = null) => {
    setIsLoading(true);

    const result = await axios({
      method: 'DELETE',
      url: `${apiHost}${path}`,
      data: body,
      header,
      validateStatus: () => true,
    });

    const resultData = result.data;

    if (resultData.status === 'error') {
      setIsLoading(false);
      setError(resultData);
    }

    if (resultData.status === 'success') {
      setIsLoading(false);
      setResponse(resultData.result);
    }
  };

  return { deleteData, setError, response, isLoading, error };
};

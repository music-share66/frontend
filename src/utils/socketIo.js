import { io } from 'socket.io-client';

export const socketIoConnection = () => {
  const socketHost = getSocketHost();
  const socket = io(
    socketHost,
    {
      transports: ['websocket'],
      autoConnect: false,
    }
  );

  socket.disconnect()
  socket.connect()

  return socket;
};

const getSocketHost = () => {
  let socketHost;
  const host = window && window.location && window.location.host;

  if(host === 'localhost:8000') {
    socketHost = 'http://localhost:3000';
  } else {
    const splitHost = host.split('.');
    splitHost[0] = `${splitHost[0]}-api`;
    const backendDomain = splitHost.join('.');
    socketHost = `https://${backendDomain}`;
  }

  return socketHost;
};

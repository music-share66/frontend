let apiHost;
const host = window && window.location && window.location.host;

if(host === 'localhost:8000') {
  apiHost = 'http://localhost:3000/api/v1';
} else {
  const splitHost = host.split('.');
  splitHost[0] = `${splitHost[0]}-api`;
  const backendDomain = splitHost.join('.');
  apiHost = `https://${backendDomain}/api/v1`;
}

export default apiHost;
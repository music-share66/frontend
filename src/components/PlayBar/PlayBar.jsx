import { useState, useEffect, useRef } from 'react';
import Hls from 'hls.js';
import { Button, Layout, Slider, Flex, Row, Col, Typography, message } from 'antd';
import { CaretRightOutlined, PauseOutlined, StepBackwardOutlined, StepForwardOutlined } from '@ant-design/icons';
import { msConvert } from '../../utils/time';
import { useGet } from '../../utils/callApi';

const { Title, Text } = Typography;
const { Footer } = Layout;

const PlayBar = ({ setSongDataToPlay, songDataToPlay, songPlayList }) => {
  const [currSongIndex, setCurrSongIndex] = useState(0);
  const [songName, setSongName] = useState('');
  const [personName, setPersonName] = useState('');
  const [durationInMs, setDurationInMs] = useState(0);
  const [isPlaying, setIsPlaying] = useState(false);
  const [progress, setProgress] = useState(0);
  const [isResourceLoadReady, setIsResourceLoadReady] = useState(false);
  const videoRef = useRef(null);
  const [messageApi, contextHolder] = message.useMessage();
  const { getData, data, error } = useGet();

  useEffect(() => {
    if (songDataToPlay) {
      const { index, songId, songName, personName, durationInMs } = songDataToPlay;
      setCurrSongIndex(index);
      setSongName(songName);
      setPersonName(personName);
      setDurationInMs(durationInMs);
      getData(`/songs/${songId}/m3u8file-url`);
    }
  }, [songDataToPlay]);

  useEffect(() => {
    if (data && Hls.isSupported()) {
      const hls = new Hls();
      hls.loadSource(data);
      hls.attachMedia(videoRef.current);
      hls.on(Hls.Events.MANIFEST_PARSED, function() {
        setIsResourceLoadReady(true);
        setIsPlaying(true);
        videoRef.current.play();
      });
    }
  }, [data]);

  useEffect(() => {
    if (error) {
      messageApi.open({
        type: 'error',
        content: 'Load song error, please try again.',
      });
    }
  }, [error]);

  useEffect(() => {
    const videoElement = videoRef.current;
    const updateProgress = () => {
      setProgress(videoElement.currentTime * 1000);
    };

    const handleEndOfSong = () => {
      setIsPlaying(false);
    };

    videoElement.addEventListener('timeupdate', updateProgress);
    videoElement.addEventListener('ended', handleEndOfSong);
    videoElement.addEventListener('ended', onClickForwardButton);

    return () => {
      videoElement.removeEventListener('timeupdate', updateProgress);
      videoElement.removeEventListener('ended', handleEndOfSong);
      videoElement.removeEventListener('ended', onClickForwardButton);
    };
  }, [isPlaying]);

  const getBackwardTargetIndex = (currentIndex) => {
    if (currentIndex <= 0 || songPlayList[currentIndex].processingStatus === 5) {
      return currentIndex;
    }
  
    return getBackwardTargetIndex(currentIndex-1);
  };

  const onClickBackwardButton = () => {
    const backwardTargetIndex = getBackwardTargetIndex(currSongIndex-1);
  
    if (songPlayList[backwardTargetIndex] !== undefined && backwardTargetIndex >= 0) {
      const { id, songName, durationInMs } = songPlayList[backwardTargetIndex];
      const songDataToPlay = {
        index: backwardTargetIndex,
        songName,
        personName,
        durationInMs,
        songId: id,
      };

      videoRef.current.pause();
      setIsResourceLoadReady(false);
      setTimeout(() => {
        setSongDataToPlay(songDataToPlay);
      }, 500);
    }
  };

  const getForwardTargetIndex = (currentIndex) => {
    if (currentIndex >= songPlayList.length || songPlayList[currentIndex].processingStatus === 5) {
      return currentIndex;
    }
  
    return getForwardTargetIndex(currentIndex + 1);
  };
  
  const onClickForwardButton = () => {
    const forwardTargetIndex = getForwardTargetIndex(currSongIndex + 1);
  
    if (songPlayList[forwardTargetIndex] !== undefined && forwardTargetIndex < songPlayList.length) {
      const { id, songName, durationInMs } = songPlayList[forwardTargetIndex];
      const songDataToPlay = {
        index: forwardTargetIndex,
        songName,
        personName,
        durationInMs,
        songId: id,
      };

      videoRef.current.pause();
      setIsResourceLoadReady(false);
      setTimeout(() => {
        setSongDataToPlay(songDataToPlay);
      }, 500);
    }
  };

  const onClickPlayPauseButton = async () => {
    if (isPlaying) {
      videoRef.current.pause();
    } else {
      videoRef.current.play();
    }

    setIsPlaying(!isPlaying);
  };

  const handleProgressChange = (value) => {
    setProgress(value);
    videoRef.current.currentTime = value / 1000;
  };

  return (
    <Footer style={{ margin: '16px 16px 0', padding: '0', background: '#ffffff', borderRadius: 10 }}>
      {contextHolder}
      <Row>
        <Col span={6}>
          <Flex justify='left' style={{ marginLeft: '10px' }} vertical>
            <Title level={3} style={{ margin: '5px 0', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', maxWidth: '100%', display: 'inline-block' }}>{songName}</Title>
            <Text type='secondary' style={{ overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', maxWidth: '100%', display: 'inline-block' }}>{personName}</Text>
          </Flex>
        </Col>
        <Col span={12} style={{ margin: '5px '}}>
          <Flex gap='middle' justify='center' align='center'>
            <Button disabled={!isResourceLoadReady} onClick={onClickBackwardButton} icon={<StepBackwardOutlined />} style={{ border: 'none', boxShadow: 'none' }}/>
            <Button disabled={!isResourceLoadReady} onClick={onClickPlayPauseButton} icon={isPlaying ? <PauseOutlined /> : <CaretRightOutlined />} style={{ border: 'none', boxShadow: 'none' }}/>
            <Button disabled={!isResourceLoadReady} onClick={onClickForwardButton} icon={<StepForwardOutlined />} style={{ border: 'none', boxShadow: 'none' }}/>
          </Flex>
          <Flex justify='space-between' align='center' gap='middle'>
            <Text style={{ whiteSpace: 'nowrap' }}>{msConvert(progress)}</Text>
            <Slider
              value={progress}
              max={durationInMs}
              tooltip={{ formatter: msConvert }}
              onChange={handleProgressChange}
              style={{ width: '100%', margin: '5px 0 0 0' }}
            />
            <Text style={{ whiteSpace: 'nowrap' }}>{msConvert(durationInMs)}</Text>
          </Flex>
        </Col>
        <Col span={6}></Col>
      </Row>
      <video ref={videoRef} style={{ display: 'none' }}></video>
    </Footer>
  );
};

export default PlayBar;

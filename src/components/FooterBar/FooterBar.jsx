import { Layout } from 'antd';

const { Footer } = Layout;

const FooterBar = () => {
  return (
    <Footer style={{ textAlign: 'center' }}>
      Music-share ©{new Date().getFullYear()} Created by Andy Liao
    </Footer>
  );
};
 
export default FooterBar;
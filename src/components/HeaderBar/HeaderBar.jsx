import { useState, useEffect, useRef } from 'react';
import { useNavigate } from 'react-router-dom';
import { Layout, Row, Col, Input, Button, Avatar, Badge, Popover } from 'antd';
import { SearchOutlined, BellOutlined } from '@ant-design/icons';
import NotificationList from './components/NotificationList';
import { socketIoConnection } from '../../utils/socketIo';

const { Header } = Layout;

const HeaderBar = ({ setSearchTrigger, searchTrigger, followingMap }) => {
  const [ws, setWs] = useState(null);
  const [notifications, setNotifications] = useState([]);
  const searchRef = useRef();
  const navigate = useNavigate();

  useEffect(() => {
    const socket = socketIoConnection();
    setWs(socket);
  }, []);

  useEffect(() => {
    if (ws && followingMap) {
      initNotificationSocket();
    }
  }, [ws, followingMap]);

  const initNotificationSocket = () => {
    ws.off('newSongNotification');

    ws.emit('notificationSocketConnect', 'notificationSocketConnect');

    ws.on('newSongNotification', (newSongNotification) => {
      if (!newSongNotification) return;
      if (!followingMap.has(newSongNotification.publishUserId)) return;
      setNotifications((prevNotifications) => [...prevNotifications, newSongNotification]);
    });
  };

  const onClickSearchButton = () => {
    const userName = searchRef.current.input.value;
    if (!userName) return;
    setSearchTrigger(!searchTrigger);
    navigate(`/user?userName=${userName}`);
  };

  useEffect(() => {
    const handleEnterKey = (event) => {
      if (event.key === 'Enter') {
        onClickSearchButton();
      }
    };

    const searchInput = searchRef.current.input;
    searchInput.addEventListener('keydown', handleEnterKey);

    return () => {
      searchInput.removeEventListener('keydown', handleEnterKey);
    };
  }, [searchRef, onClickSearchButton]);

  return (
    <Header style={{ background: '#ffffff'}}>
      <Row style={{ width: '100%', height: '100%' }} align='middle'>
        <Col span={8}></Col>
        <Col span={8}>
          <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <Input placeholder='user name' ref={searchRef} style={{ borderRadius: 50, background: '#f5f5f5' }} />
            <Button onClick={onClickSearchButton} icon={<SearchOutlined />} style={{ border: 'none', boxShadow: 'none' }} />
          </div>
        </Col>
        <Col span={8} style={{ display: 'flex', justifyContent: 'end' }}>
          <Popover placement='bottomRight' content={<NotificationList notifications={notifications} />} title='Notification' trigger='click' >
            <Badge count={notifications.length}>
              <Avatar icon={<BellOutlined />} />
            </Badge>
          </Popover>
        </Col>
      </Row>
    </Header>
  );
};

export default HeaderBar;

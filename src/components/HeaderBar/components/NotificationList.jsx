import { List } from 'antd';
const { Item } = List;

const NotificationList = ({ notifications }) => {
  return (
    <List
      style={{ height: '300px', overflow: 'auto' }}
      size='large'
      bordered
      dataSource={notifications}
      renderItem={(notification) => <Item style={{ width: '300px' }}>{notification.message}</Item>}
    />
  );
};

export default NotificationList;

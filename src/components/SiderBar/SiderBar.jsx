import { Flex, Button, Layout } from 'antd';
import { Link } from 'react-router-dom';
import { useGet, usePost } from '../../utils/callApi'; 
import { useState, useEffect } from 'react';

const { Sider } = Layout;

const Siderbar = ({ setUserId = null, setPersonName = null, setFollowingList = null, setFollowingMap = null}) => {
  const [isSignin, setIsSignin] = useState(false);
  const [userName, setUserName] = useState(null);
  const [queryUserId, setQueryUserId] = useState('');
  const { getData, data, error } = useGet();
  const { postData } = usePost();

  useEffect(() => {
    getData('/signin/user');
  }, []);

  useEffect(() => {
    if (data) {
      const { signinUser } = data;

      if (setUserId && signinUser.userId) {
        setUserId(signinUser.userId);
        setQueryUserId(signinUser.userId);
      }

      if (setPersonName && signinUser.userName) {
        setPersonName(signinUser.userName);
      }

      if (setFollowingList && signinUser.followingUser) {
        setFollowingList(signinUser.followingUser);
      }

      if (setFollowingMap && signinUser.followingUser) {
        const followingMap = new Map();
        signinUser.followingUser.forEach((following) => {
          const { id: followingId, followee } = following;
          const { id } = followee;
          followingMap.set(id, followingId);
        });
        setFollowingMap(followingMap);
      }

      setUserName(signinUser.userName);
      setIsSignin(true);
    }
  }, [data]);

  useEffect(() => {
    if (error) {
      setIsSignin(false);
    }
  }, [error]);

  const clickSignoutButton = () => {
    postData('/signout');
    setIsSignin(false);
  };

  return (
    <Sider style={{ backgroundColor: '#ffffff', boxShadow: '5px 0px 10px rgba(0, 0, 0, 0.1)', overflow: 'auto', height: '100vh', position: 'fixed', left: 0, top: 0, bottom: 0 }}>
      <Flex vertical={true} align='center' justify='space-between' style={{ height: '100%' }}>
        <Flex gap='middle' vertical={true} justify='start' align='center' style={{ paddingTop: '30px', paddingBottom: '30px'}}>
          <Link to='/'>Home</Link>
          {
            isSignin ? (
              <>
                <Link to={`/personal?userId=${queryUserId}`}><p>{userName}</p></Link>
                <Link to='/following'>Following</Link>
              </>
            ) : (
              <Link to='/signin'>Signin</Link>
            )
          }
        </Flex>
          {
            isSignin ? (
              <Button style={{ marginBottom: '30px'}} onClick={clickSignoutButton}>Sign out</Button>
            ) : (
              null
            )
          }
      </Flex>
    </Sider>
  );
};
 
export default Siderbar;

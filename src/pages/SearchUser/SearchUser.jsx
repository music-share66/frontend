import { useState, useEffect } from 'react';
import { Typography, Divider, List, Flex } from 'antd';
import User from './components/User.jsx';
import { useGet } from '../../utils/callApi';

const { Title } = Typography;

const SearchUser = ({ searchTrigger }) => {
  const [userName, setUserName] = useState('');
  const [user, setUser] = useState([]);
  const { getData, data } = useGet();

  useEffect(() => {
    const currentUrl = window.location.href;
    const url = new URL(currentUrl);
    const searchUserName = url.searchParams.get('userName');
    setUserName(searchUserName);
    getData(`/users/name/${searchUserName}`);
  }, [searchTrigger]);

  useEffect(() => {
    if (data) {
      setUser(data);
    }
  }, [data]);

  return (
    <>
      <Flex justify='space-between' align='center' >
        <Title level={2} style={{ margin: '0' }} >Search user - {userName}</Title>
      </Flex>
      <Divider />
      <List
        bordered
        dataSource={user}
        renderItem={(item, index) => (
          <List.Item>
            <User index={index} user={item} />
          </List.Item>
        )}
      />
    </>
  );
};

export default SearchUser;

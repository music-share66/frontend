import { Typography, Flex } from 'antd';
import { Link } from 'react-router-dom';

const User = ({ index, user }) => {
  const { id, userName } = user;
  const { Title } = Typography;

  return (
    <Link to={`/personal?userId=${id}`}>
      <Flex justify='space-between' align='center'>
        <Title level={4} style={{ margin: 0 }} >{index+1}. {userName}</Title>
      </Flex>
    </Link>
  );
};

export default User;

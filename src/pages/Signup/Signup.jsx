import { Flex, Row, Col, Input, Button, Modal } from 'antd';
import { Link, useNavigate } from 'react-router-dom';
import { useEffect, useRef } from 'react';
import { usePost } from '../../utils/callApi'; 

const Signup = () => {
  const inputEmailRef = useRef();
  const inputPasswordRef = useRef();
  const inputRepeatPasswordRef = useRef();
  const inputUserNameRef = useRef();
  const navigate = useNavigate();
  const { postData, setError, response, error } = usePost();

  useEffect(() => {
    inputEmailRef.current.focus();
  }, [])

  const clickSignupButton = async () => {
    const email = inputEmailRef.current.input.value;
    const password = inputPasswordRef.current.input.value;
    const repeatPassword = inputRepeatPasswordRef.current.input.value;
    const userName = inputUserNameRef.current.input.value;

    if (email === '' || password === '' || userName === '' || repeatPassword === '') {
      Modal.error({
        title: 'Signup error',
        content: 'email or password or userName or repeatPassword is empty',
      });
      return;
    }

    const data = {
      email,
      password,
      repeatPassword,
      userName,
    }

    postData('/signup', data);
  }

  useEffect(() => {
    if (response) {
      Modal.info({
        title: 'Signup success',
        content: `Signup success !!! user : ${response} created`,
      });
      navigate('/signin');
    }
  }, [response, navigate]);

  useEffect(() => {
    if (error) {
      Modal.error({
        title: 'Signup error',
        content: error.message,
      });
      setError(null);
    }
  }, [error, setError]);

  return (
    <Row>
      <Col span={8}></Col>
      <Col span={8}>
        <Flex gap='middle' vertical={true} justify='center' align='center'>
          <h1>Signup</h1>
          <Input placeholder='email' style={{ width: '50%' }} ref={inputEmailRef}/>
          <Input placeholder='password' style={{ width: '50%' }} ref={inputPasswordRef}/>
          <Input placeholder='repeat password' style={{ width: '50%' }} ref={inputRepeatPasswordRef}/>
          <Input placeholder='username' style={{ width: '50%' }} ref={inputUserNameRef}/>
          <Flex gap='middle' vertical style={{ width: '50%' }}>
            <Button type='primary' block={true} onClick={clickSignupButton}>Signup</Button>
            <Link to='/signin'>
              <Button block={true}>Signin</Button>
            </Link>
            <Link to='/'>
              <Button block={true}>Home</Button>
            </Link>
          </Flex>
        </Flex> 
      </Col>
      <Col span={8}></Col>
    </Row>
  );
}

export default Signup;

import { useState } from 'react';
import { Routes, Route } from 'react-router-dom';
import { Layout } from 'antd';
import HeaderBar from '../../components/HeaderBar/HeaderBar.jsx';
import Siderbar from '../../components/SiderBar/SiderBar.jsx';
import PlayBar from '../../components/PlayBar/PlayBar.jsx';
import FooterBar from '../../components/FooterBar/FooterBar.jsx';
import Main from '../Main/Main.jsx';
import Personal from '../Personal/Personal.jsx';
import SearchUser from '../SearchUser/SearchUser.jsx';
import UploadSong from '../UploadSong/UploadSong.jsx';
import Following from '../Following/Following.jsx';

const { Content } = Layout;

const Index = () => {
  const [userId, setUserId] = useState(null);
  const [personName, setPersonName] = useState(null);
  const [followingList, setFollowingList] = useState([]);
  const [followingMap, setFollowingMap] = useState(new Map());
  const [songDataToPlay, setSongDataToPlay] = useState(null);
  const [songPlayList, setSongPlayList] = useState(null);
  const [searchTrigger, setSearchTrigger] = useState(false);

  return (
    <Layout hasSider>
      <Siderbar setUserId={setUserId} setPersonName={setPersonName} setFollowingList={setFollowingList} setFollowingMap={setFollowingMap}/>
      <Layout style={{ marginLeft: 200, height: '100vh' }}>
        <HeaderBar setSearchTrigger={setSearchTrigger} searchTrigger={searchTrigger} followingMap={followingMap}/>
          <Content style={{ margin: '16px 16px 0', overflowX: 'hidden', overflowY: 'auto', padding: 24, background: '#ffffff', borderRadius: 10}}>
            <Routes>
              <Route path='/' element={<Main />} />
              <Route path='/personal' 
                element={
                  <Personal 
                    userId={userId}
                    followingList={followingList}
                    setFollowingList={setFollowingList}
                    followingMap={followingMap}
                    setSongDataToPlay={setSongDataToPlay}
                    setSongPlayList={setSongPlayList}
                  />
                }
              />
              <Route path='/personal/upolad-song' element={<UploadSong userId={userId} personName={personName} />} />
              <Route path='/user' element={<SearchUser searchTrigger={searchTrigger}/>}></Route>
              <Route path='/following' element={<Following followingList={followingList} />} />
            </Routes>
          </Content>
        <PlayBar setSongDataToPlay={setSongDataToPlay} songDataToPlay={songDataToPlay} songPlayList={songPlayList}/>
        <FooterBar />
      </Layout>
    </Layout>
  );
};

export default Index;

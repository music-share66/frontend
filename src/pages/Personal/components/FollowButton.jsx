import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button, Modal} from 'antd';
import { usePost } from '../../../utils/callApi';

const FollowButton = ({
  userId,
  songsUserId,
  setIsFollowing,
  setFollowingId,
  followingList,
  setFollowingList,
  followingMap,
}) => {
  const { postData, response, error, setError } = usePost();
  const navigate = useNavigate();

  const onClickFollowButton = () => {
    const data = {
      followeeId: songsUserId
    }

    postData(`/users/${userId}/following`, data);
  };

  useEffect(() => {
    if (response) {
      setIsFollowing(true);
      setFollowingList([...followingList, response]);
      setFollowingId(response.id)
      followingMap.set(songsUserId, response.id);
    }
  }, [response]);

  useEffect(() => {
    if (error) {
      if (error.statusCode === 401 || error.statusCode === 403) {
        Modal.error({
          title: 'Follow user error',
          content: 'You are not authorized to follow this user. Please sign in.',
        });
        setError(null);
        navigate('/signin');
      } else {
        Modal.error({
          title: 'Follow user error',
          content: error.message,
        });
        setError(null);
      }
    }
  }, [error]);

  return (
    <Button type='primary' size='small' onClick={onClickFollowButton} style={{ borderRadius: 50 }}>Follow</Button>
  );
};

export default FollowButton;

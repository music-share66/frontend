import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Typography, Button, Flex, Row, Col, Modal } from 'antd';
import { DeleteFilled } from '@ant-design/icons';
import { msConvert } from '../../../utils/time';
import { useDelete } from '../../../utils/callApi';
const { Text, Title } = Typography;

const Song = ({ songs, index, personName, isOwner, songMetaDatas, setSongDataToPlay, setSongPlayList, setSongs }) => {
  const { deleteData, setError, response, error } = useDelete();
  const { id, songName, durationInMs, processingStatus } = songMetaDatas;
  const navigate = useNavigate();

  const clickSongToPlay = () => {
    const songDataToPlay = {
      index,
      songName,
      personName,
      durationInMs,
      songId: id,
    };

    setSongPlayList(songs);
    setSongDataToPlay(songDataToPlay);
  };

  const clickDeleteFileButton = () => {
    Modal.confirm({
      title: 'Delete confirm',
      content: `Sure to delete ${songName}? Can't be undo!!!`,
      onOk() {
        deleteData(`/songs/${id}`);
      },
    });
  };

  useEffect(() => {
    if (response) {
      Modal.info({
        title: 'Deleting',
        content: `Song deleting`,
      });
      setSongs(prevSongs => prevSongs.filter(song => song.id !== id));
    }
  }, [response]);

  useEffect(() => {
    if (error) {
      if (error.statusCode === 401 || error.statusCode === 403) {
        Modal.error({
          title: 'Delete error',
          content: 'You are not authorized to delete this song. Please sign in.',
        });
        setError(null);
        navigate('/signin');
      } else {
        Modal.error({
          title: 'Delete error',
          content: error.message,
        });
        setError(null);
      }
    }
  }, [error, setError, navigate]);

  const isProcessingComplete = processingStatus === 4 || processingStatus === 5;
  const duration = msConvert(durationInMs);

  return (
    <>
      <div style={{ width: '100%' }}>
        <Row >
          <Flex style={{ width: '100%' }}>
            {isProcessingComplete ? (
              <Button type='link' onClick={clickSongToPlay} style={{ width: '100%', padding: 0, border: 'none', textAlign: 'left' }}>
                <Col span={24} >
                  <Flex justify='space-between' align='center'>
                    <Col span={8}>
                      <Title level={4} style={{ margin: 0 }} >{index+1}. {songName}</Title>
                    </Col>
                    <Col span={8}>
                      <Text type='secondary'>{personName}</Text>
                    </Col>
                    <Col span={4}>
                      <Text type='secondary'> - {duration}</Text>
                    </Col>
                    <Col span={4}></Col>
                  </Flex>
                </Col>
              </Button>
            ) : (
              <span style={{ width: '100%', display: 'inline-block' }}>
                <Col span={24} >
                  <Flex justify='space-between' align='center'>
                    <Col span={8}>
                      <Title level={4} style={{ margin: 0, color: 'gray'}}>{index+1}. {songName}</Title>
                    </Col>
                    <Col span={8}>
                      <Text type='secondary'>{personName}</Text>
                    </Col>
                    <Col span={4}>
                      <Text type='secondary'> - {duration}</Text>
                    </Col>
                    <Col span={4}>
                      <Text type='secondary'>Processing</Text>
                    </Col>
                  </Flex>
                </Col>
              </span>
            )}
            <Col span={4} style={{ textAlign: 'right' }}>
              {
                isOwner ? (
                  isProcessingComplete ? (
                    <Button icon={<DeleteFilled style={{ color: 'red' }} onClick={clickDeleteFileButton}/>} />
                  ) : (
                    <Button disabled icon={<DeleteFilled style={{ color: 'gray' }} />} />
                  )
                ) : null
              }
            </Col>
          </Flex>
        </Row>
      </div>
    </>
  );
};

export default Song;

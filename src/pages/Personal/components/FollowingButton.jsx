import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button, Modal} from 'antd';
import { useDelete } from '../../../utils/callApi';

const FollowingButton = ({
  userId,
  followingId,
  setIsFollowing,
  songsUserId,
  followingList,
  setFollowingList,
  followingMap,
}) => {
  const { deleteData, response, error, setError } = useDelete();
  const navigate = useNavigate();

  const onClickfollowingButton = () => {
    deleteData(`/users/${userId}/following/${followingId}`);
  };

  useEffect(() => {
    if (response) {
      setIsFollowing(false);
      setFollowingList(followingList.filter((following) => following.id !== followingId));
      followingMap.delete(songsUserId);
    }
  }, [response]);

  useEffect(() => {
    if (error) {
      if (error.statusCode === 401 || error.statusCode === 403) {
        Modal.error({
          title: 'Unfollow user error',
          content: 'You are not authorized to unfollow this user. Please sign in.',
        });
        setError(null);
        navigate('/signin');
      } else {
        Modal.error({
          title: 'Unfollow user error',
          content: error.message,
        });
        setError(null);
      }
    }
  }, [error]);

  return (
    <Button size='small' onClick={onClickfollowingButton} style={{ borderRadius: 50 }}>Following</Button>
  );
};

export default FollowingButton;

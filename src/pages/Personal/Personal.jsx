import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Typography, Divider, List, Button, Flex } from 'antd';
import Song from './components/Song.jsx';
import { useGet, usePut } from '../../utils/callApi';
import FollowButton from './components/FollowButton.jsx';
import FollowingButton from './components/FollowingButton.jsx';

const { Title } = Typography;

const Personal = ({
  userId,
  followingList,
  setFollowingList,
  followingMap,
  setSongDataToPlay,
  setSongPlayList
}) => {
  const [songsUserId, setSongsUserId] = useState('');
  const [isOwner, setIsOwner] = useState(false);
  const [isFollowing, setIsFollowing] = useState(false);
  const [followingId, setFollowingId] = useState('');
  const [personName, setPersonName] = useState('');
  const [songs, setSongs] = useState([]);
  const { getData, data } = useGet();
  const { putData } = usePut();

  useEffect(() => {
    const currentUrl = window.location.href;
    const url = new URL(currentUrl);
    const queryUserId = url.searchParams.get('userId');
    getData(`/users/${queryUserId}/songs`);
  }, []);

  useEffect(() => {
    if (data) {
      const { id, userName, songs } = data;
      setSongsUserId(id);
      setPersonName(userName);
      setSongs(songs);
    }
  }, [data]);

  useEffect(() => {
    if (songsUserId && userId) {
      const isOwner = songsUserId === userId;
      setIsOwner(isOwner);
      const isFollowing = followingMap.has(songsUserId);
      setIsFollowing(isFollowing);
      const followingId = followingMap.get(songsUserId);
      setFollowingId(followingId);
    }
  }, [songsUserId, userId, followingMap]);

  const onClickRefreshProcessingStatusButton = () => {
    putData('/songs/encode-status');
  };

  return (
    <>
      <Flex justify='space-between'>
        <Flex gap='middle' align='flex-end'>
          <Title level={2} style={{ margin: '0' }} >{personName}</Title>
          {
            !isOwner && (
              isFollowing ?
                <FollowingButton
                  userId={userId}
                  followingId={followingId}
                  setIsFollowing={setIsFollowing}
                  songsUserId={songsUserId}
                  followingList={followingList}
                  setFollowingList={setFollowingList}
                  followingMap={followingMap}
                /> :
                <FollowButton
                  userId={userId}
                  songsUserId={songsUserId}
                  setIsFollowing={setIsFollowing}
                  setFollowingId={setFollowingId}
                  followingList={followingList}
                  setFollowingList={setFollowingList}
                  followingMap={followingMap}
                />
            )
          }
        </Flex>
        {
          isOwner &&
            <Flex>
              <Button onClick={onClickRefreshProcessingStatusButton} style={{ marginRight: '10px' }}>Refresh processing status</Button>
              <Button><Link to='/personal/upolad-song'>Upload song</Link></Button>
            </Flex>
        }
      </Flex>
      <Divider />
      <List
        bordered
        dataSource={songs}
        renderItem={(item, index) => (
          <List.Item>
            <Song songs={songs} index={index} personName={personName} isOwner={isOwner} songMetaDatas={item} setSongDataToPlay={setSongDataToPlay} setSongPlayList={setSongPlayList} setSongs={setSongs}/>
          </List.Item>
        )}
      />
    </>
  );
};

export default Personal;

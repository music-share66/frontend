import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button, Modal } from 'antd';
import { usePost } from '../../../utils/callApi';

export const SigninButton = ({ inputEmailRef, inputPasswordRef }) => {
  const navigate = useNavigate();
  const { postData, setError, response, error } = usePost();

  const clickSigninButton = async () => {
    const email = inputEmailRef.current.input.value;
    const password = inputPasswordRef.current.input.value;

    const data = {
      email,
      password
    }

    postData('/signin', data);
  }

  useEffect(() => {
    if (response) {
      navigate('/');
    }
  }, [response, navigate]);

  useEffect(() => {
    if (error) {
      Modal.error({
        title: 'Signin error',
        content: error.message,
      });
      setError(null);
    }
  }, [error, setError]);

  useEffect(() => {
    const handleEnterKey = (event) => {
      if (event.key === 'Enter') {
        clickSigninButton();
      }
    }

    const emailInput = inputEmailRef.current.input;
    const passwordInput = inputPasswordRef.current.input;

    emailInput.addEventListener('keydown', handleEnterKey);
    passwordInput.addEventListener('keydown', handleEnterKey);

    return () => {
      emailInput.removeEventListener('keydown', handleEnterKey);
      passwordInput.removeEventListener('keydown', handleEnterKey);
    };
  }, [inputEmailRef, inputPasswordRef]);

  return (
    <>
      <Button type='primary' block={true} onClick={clickSigninButton}>Signin</Button>
    </>
  );
};

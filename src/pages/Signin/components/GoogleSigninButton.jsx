import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button, Modal } from 'antd'
import { GoogleOutlined } from '@ant-design/icons';
import { useGet } from '../../../utils/callApi';

export const GoogleSigninButton = () => {
  const navigate = useNavigate();
  const { getData, setError, data, error } = useGet();

  const clickGoogleSigninButton = () => {
    getData('/google/signin');
  };

  useEffect(() => {
    if (data) {
      const { googleOauthConsentScreenUrl } = data;
      window.location.href = googleOauthConsentScreenUrl;
    }
  }, [data, navigate]);

  useEffect(() => {
    if (error) {
      Modal.error({
        title: 'Signin error',
        content: error.message,
      });
      setError(null);
    }
  }, [error, setError]);

  return (
    <>
      <Button type='primary' block={true} icon={<GoogleOutlined />} onClick={clickGoogleSigninButton}>
        Sign in with Google
      </Button>
    </>
  );
};

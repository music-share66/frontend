import { useEffect, useRef } from 'react';
import { Link } from 'react-router-dom';
import { Flex, Row, Col, Input, Button } from 'antd';
import { SigninButton } from './components/SigninButton';
import { GoogleSigninButton } from './components/GoogleSigninButton';

const Signin = () => {
  const inputEmailRef = useRef();
  const inputPasswordRef = useRef();

  useEffect(() => {
    inputEmailRef.current.focus();
  }, [])

  return (
    <Row>
      <Col span={8}></Col>
      <Col span={8}>
        <Flex gap='middle' vertical={true} justify='center' align='center'>
          <h1>Signin</h1>
          <Input placeholder='email' style={{ width: '50%' }} ref={inputEmailRef}/>
          <Input placeholder='password' style={{ width: '50%' }} ref={inputPasswordRef}/>
          <Flex gap='middle' vertical style={{ width: '50%' }}>
            <SigninButton inputEmailRef={inputEmailRef} inputPasswordRef={inputPasswordRef}/>
            <GoogleSigninButton />
            <Link to='/signup'>
              <Button block={true}>Signup</Button>
            </Link>
            <Link to='/'>
              <Button block={true}>Home</Button>
            </Link>
          </Flex>
        </Flex> 
      </Col>
      <Col span={8}></Col>
    </Row>
  );
}

export default Signin;

import { useState, useRef, useEffect } from 'react';
import { Typography, Divider, Button, Flex, Input, Upload, Row, Col, Modal } from 'antd';
import { InboxOutlined } from '@ant-design/icons'
import { usePost } from '../../utils/callApi';
import { useNavigate } from 'react-router-dom';
const { Title } = Typography;
const { Dragger } = Upload;

const UploadSong = ({ userId, personName }) => {
  const [songFile, setSongFile] = useState(null);
  const [pendingModal, setPendingModal] = useState(null);
  const songNameRef = useRef(null);
  const navigate = useNavigate();
  const { postData, setError, response, isLoading, error } = usePost();

  const beforeUpload = (file) => {
    setSongFile(file);
    return false;
  };

  const onClickUploadButton = () => {
    if (!songFile || !songNameRef.current.input.value) {
      Modal.error({
        title: 'Upload error',
        content: 'Song name and song file cannot be null.',
      });
      return;
    }

    const formData = new FormData();
    formData.append('songFile', songFile);
    formData.append('songName', songNameRef.current.input.value);
    const headers = {
      'Content-Type': 'multipart/form-data',
    }

    postData('/songs', formData, headers);
  }

  useEffect(() => {
    if (isLoading) {
      const modalInstance = Modal.info({
        okButtonProps: {
          disabled: true,
          style: { display: 'none' },
        },
        title: 'Pending',
        content: `Waiting for processing start. Please do not refresh or close this page.`,
      });

      setPendingModal(modalInstance);
    }
  }, [isLoading]);

  useEffect(() => {
    if (response) {
      pendingModal.destroy();
      Modal.info({
        title: 'Processing',
        content: `New song creating`,
      });
      navigate(`/personal?userId=${userId}`);
    }
  }, [response, navigate]);

  useEffect(() => {
    if (error) {
      pendingModal.destroy();
      if (error.statusCode === 401 || error.statusCode === 403) {
        Modal.error({
          title: 'Upload error',
          content: 'You are not authorized to upload song. Please sign in.',
        });
        setError(null);
        navigate('/signin');
      } else {
        Modal.error({
          title: 'Upload error',
          content: error.message,
        });
        setError(null);
      }
    }
  }, [error, setError, navigate]);

  return (
    <>
      <Title level={2} style={{ margin: '0' }} >{personName} - Upolad song</Title>
      <Divider style={{ margin: '24px 0 0 0' }}/>
      <Row>
        <Col span={6}></Col>
        <Col span={12}>
          <Flex align='center' style={{ flexWrap: 'nowrap' }}>
              <Title level={3} style={{ margin: '20px 0', whiteSpace: 'nowrap', marginRight: '10px' }}>Song name :</Title>
              <Input placeholder='song name' ref={songNameRef} style={{ width: '100%' }}/>
          </Flex>
          <Dragger
            name='songFile'
            maxCount={1}
            beforeUpload={beforeUpload}
          >
            <p className="ant-upload-drag-icon">
              <InboxOutlined />
            </p>
            <p className="ant-upload-text">Click or drag file to this area to upload</p>
            <p className="ant-upload-hint">MP4 Only</p>
          </Dragger>
          <Flex justify='end' style={{ margin: '10px 0' }}>
            <Button type='primary' onClick={onClickUploadButton}>Upload</Button>  
          </Flex>
        </Col>
        <Col span={6}></Col>
      </Row>
    </>
  );
};

export default UploadSong;

import { Typography, Divider, List, Flex } from 'antd';
import User from './components/User.jsx';

const { Title } = Typography;

const Following = ({ followingList }) => {
  return (
    <>
      <Flex justify='space-between' align='center' >
        <Title level={2} style={{ margin: '0' }} >Following</Title>
      </Flex>
      <Divider />
      <List
        bordered
        dataSource={followingList}
        renderItem={(item, index) => (
          <List.Item>
            <User index={index} followee={item.followee} />
          </List.Item>
        )}
      />
    </>
  );
};

export default Following;

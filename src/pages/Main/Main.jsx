import React from 'react';
import { Row, Col, Divider } from 'antd';

const Main = () => {
  return (
    <div style={{ padding: 24 }}>
      <h4>歡迎來到 Music Share。說明文件提供中文版本和英文版本。請選擇您熟悉的語言進行閱讀</h4>
      <h4>Welcome to Music Share. The documentation is available in both Chinese and English. Please choose the language you are more comfortable with.</h4>
      <Divider>中文版</Divider>
      <h1>系統介紹</h1>
      <p>
        歡迎來到 Music share 這個平台, 這個 Side project 可以讓大家在這邊分享自己的音樂,
        並且關注自己喜歡的音樂人, 下方會有詳細的使用介紹, 感謝點進來的你 / 妳
      </p>
      <h1>使用介紹</h1>
      <Row gutter={16}>
        <Col span={8}>
          <div style={{ padding: '0 20px' }}>
            <h2>建立帳號</h2>
            <p>您可以透過以下兩種方式在主頁上建立帳號：</p>
            <div style={{ marginLeft: '20px' }}>
              <h3>Email / Password 註冊</h3>
              <div style={{ marginLeft: '20px' }}>
                <p>1. 點選“Signup”進入註冊頁面。</p>
                <p>2. 輸入您的 email、password 和 username。</p>
                <p>3. 點擊“Signup”按鈕即可建立帳號。</p>
                <p>4. 之後，您可以使用該帳號登入。</p>
              </div>
              <h3>Google 註冊</h3>
              <div style={{ marginLeft: '20px' }}>
                <p>1. 點擊“Sign in with Google”。</p>
                <p>2. 完成授權後即可建立帳號並直接登入。</p>
              </div>
            </div>
          </div>
        </Col>
        <Col span={8}>
          <div style={{ padding: '0 20px' }}>
            <h2>管理音樂</h2>
            <p>點擊左側菜單中的用戶名稱即可進入音樂管理頁面。</p>
            <div style={{ marginLeft: '20px' }}>
              <h3>上傳音樂</h3>
              <div style={{ marginLeft: '20px' }}>
                <p>1. 點擊右上角的“Upload song”按鈕。</p>
                <p>2. 輸入音樂名稱並上傳檔案（限 mp4 格式）。可以使用以下連結裡的檔案</p>
                <a href="https://drive.google.com/drive/folders/1l7XHf_pTnSYqDQ210SLbcXM8K6RjPo0o?usp=drive_link" target="_blank">Music share demo file</a>
                <p>3. 點擊“Upload”後，頁面將自動跳回音樂管理頁面。</p>
                <p>4. 重新整理頁面後，您會看到剛上傳的音樂顯示為“Processing”。</p>
                <p>5. 約 3 分鐘後，點擊右上角的“Refresh processing status”按鈕，重新整理頁面即可看到音樂已處理完成。</p>
              </div>
              <h3>播放音樂</h3>
              <div style={{ marginLeft: '20px' }}>
                <p>點擊未顯示“Processing”狀態的音樂即可播放。</p>
              </div>
              <h3>刪除音樂</h3>
              <div style={{ marginLeft: '20px' }}>
                <p>1. 在音樂列表中，點擊想要刪除的音樂右側的垃圾桶圖標。</p>
                <p>2. 點擊後，會彈出確認框。</p>
                <p>3. 點擊“Confirm”後即可刪除音樂。</p>
                <p><b>請注意：</b>刪除操作無法恢復。</p>
              </div>
            </div>
          </div>
        </Col>
        <Col span={8}>
          <div style={{ padding: '0 20px' }}>
            <h2>關注其他用戶</h2>
            <p>點擊左側菜單中的“Following”即可進入關注管理頁面。</p>
            <div style={{ marginLeft: '20px' }}>
              <h3>關注用戶</h3>
              <div style={{ marginLeft: '20px' }}>
                <p>1. 在搜索框中輸入想要查找的用戶名稱。</p>
                <p>可直接搜尋用戶 'demo01' 並享受音樂</p>
                <p>2. 點擊搜尋，找到目標用戶後。</p>
                <p>3. 進入其頁面並點擊“Follow”按鈕即可關注。</p>
                <p>4. 當關注的用戶發布新音樂時，右上角小鈴鐺會彈出通知。</p>
              </div>
              <h3>取消關注用戶</h3>
              <div style={{ marginLeft: '20px' }}>
                <p>1. 進入想要取消關注的用戶頁面。</p>
                <p>2. 點擊“Following”按鈕即可取消關注。</p>
              </div>
            </div>
          </div>
        </Col>
      </Row>
      <Divider>English version</Divider>
      <h1>System Introduction</h1>
      <p>
        Welcome to Music Share, a platform where you can share your music and follow your favorite musicians. 
        Detailed usage instructions are provided below. Thank you for visiting.
      </p>
      <h1>Usage Guide</h1>
      <Row gutter={16}>
        <Col span={8}>
          <div style={{ padding: '0 20px' }}>
            <h2>Create Account</h2>
            <p>You can create an account on the main page through the following two methods:</p>
            <div style={{ marginLeft: '20px' }}>
              <h3>Email / Password Signup</h3>
              <div style={{ marginLeft: '20px' }}>
                <p>1. Click "Signup" to go to the registration page.</p>
                <p>2. Enter your email, password, and username.</p>
                <p>3. Click "Signup" to create an account.</p>
                <p>4. You can then log in with this account.</p>
              </div>
              <h3>Google Signup</h3>
              <div style={{ marginLeft: '20px' }}>
                <p>1. Click "Signin with Google".</p>
                <p>2. Complete authorization to create an account and log in directly.</p>
              </div>
            </div>
          </div>
        </Col>
        <Col span={8}>
          <div style={{ padding: '0 20px' }}>
            <h2>Manage Music</h2>
            <p>Click on your username in the left menu to enter the music management page.</p>
            <div style={{ marginLeft: '20px' }}>
              <h3>Upload Music</h3>
              <div style={{ marginLeft: '20px' }}>
                <p>1. Click the "Upload song" button in the top right corner.</p>
                <p>2. Enter the music name and upload the file (limited to mp4 format). You can use the file at the following link.</p>
                <a href="https://drive.google.com/drive/folders/1l7XHf_pTnSYqDQ210SLbcXM8K6RjPo0o?usp=drive_link" target="_blank">Music share demo file</a>
                <p>3. After clicking "Upload", the page will automatically redirect to the music management page.</p>
                <p>4. After refreshing the page, you will see the newly uploaded music displayed as "Processing".</p>
                <p>5. After about 3 minutes, click the "Refresh processing status" button in the top right corner to see that the music has been processed.</p>
              </div>
              <h3>Play Music</h3>
              <div style={{ marginLeft: '20px' }}>
                <p>Click on music without the "Processing" status to play.</p>
              </div>
              <h3>Delete Music</h3>
              <div style={{ marginLeft: '20px' }}>
                <p>1. In the music list, click the trash can icon next to the music you want to delete.</p>
                <p>2. Click, a confirmation box will appear.</p>
                <p>3. Click "Confirm" to delete the music.</p>
                <p><b>Please note:</b> Deleting music cannot be undone.</p>
              </div>
            </div>
          </div>
        </Col>
        <Col span={8}>
          <div style={{ padding: '0 20px' }}>
            <h2>Follow Other Users</h2>
            <p>Click on "Following" in the left menu to enter the following management page.</p>
            <div style={{ marginLeft: '20px' }}>
              <h3>Follow User</h3>
              <div style={{ marginLeft: '20px' }}>
                <p>1. Enter the username you want to search for in the search box.</p>
                <p>You can directly search for the user 'demo01' and enjoy the music.</p>
                <p>2. Click "Search" and find the target user.</p>
                <p>3. Enter their page and click the "Follow" button to follow them.</p>
                <p>4. When the followed user releases new music, a notification will pop up in the top right corner.</p>
              </div>
              <h3>Unfollow User</h3>
              <div style={{ marginLeft: '20px' }}>
                <p>1. Go to the page of the user you want to unfollow.</p>
                <p>2. Click the "Following" button to unfollow them.</p>
              </div>
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default Main;
